import Vue from 'vue'
import Router from 'vue-router'

import Home from "../pages/Home";
import Requirements from "../pages/Requirements";
import Downloads from "../pages/Downloads";
import Servers from "../pages/Servers/Index.vue";
import Server from "../pages/Servers/Server.vue";
import Mod from "../pages/Mod";
import Modpack from "../pages/Modpack";
import Login from "../pages/Login";
//import Users from "../pages/Users";


Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            name: 'home',
            path: '/',
            component: Home
        },
        {
            name: 'req',
            path: '/requirements',
            component: Requirements
        },
        {
            name: 'downloads',
            path: '/downloads',
            component: Downloads
        },
        {
            name: 'servers',
            path: '/server',
            component: Servers
        },
        
        {
            path: '/server/:server_id',
            component: Server
        },
        {
            name: 'servers',
            path: '/server',
            component: Servers
        },
        {
            name: 'mods',
            path: '/mod',
            component: Mod
        },
        {
            name: 'modpacks',
            path: '/modpack',
            component: Modpack
        },
        {
            name: 'login',
            path: '/login',
            component: Login
        }
    ]
})
