<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Black Hawk Minecraft</title>

        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
	<script type="text/javascript">
	  var _paq = _paq || [];
	  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
	    var u="//matomo.blackhawkelectronics.com/";
	    _paq.push(['setTrackerUrl', u+'piwik.php']);
	    _paq.push(['setSiteId', '3']);
	    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
        <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="site.css">

    </head>
    <body id="page-top">
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="#"><div class="bhe">BlackHawk</div> <div class="minecraft">Minecraft</div></a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#page-top">Home</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#requirements">Requirements</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#instructions">Instructions</a></li>
                </ul>
            </div>
        </nav>
        
        <!-- Initial view -->
        <header class="intro text-center text-white d-flex minh">
            <div class="container my-auto">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h1 class="text-uppercase">
                            <strong>Craft with Friends</strong>
                        </h1>
                        <hr>
                    </div>
                    <div class="col-lg-8 mx-auto">
                        <p class="text-faded mb-5">Play on the BlackHawk minecraft server network!</p>
                        <h3>Featured Servers:</h3>
                        <dl class="row text-left">
                            @foreach($servers as $server)
                            <dt class="col-sm-3 offset-sm-2"><span data-target="{{$server->id}}" class="statusdot" style="background-color:#ccc;"></span> {{$server->name}}</dt><dd class="col-sm-7">{{$server->description}}</dd>
                            @endforeach
                        </dl>
                        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#requirements">Check the requirements</a>
                    </div>
                </div>
            </div>
        </header>
        
        <!-- Requirements -->
        <section class="bg-primary minh" id="requirements">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <h2 class="section-heading text-white">The requirements...</h2>
                        <hr class="light my-4">
                        <p class="text-faded mb-4">Here's what you need</p>
                        <ol>
                            <li>A Minecraft license</li>
                            <li>A copy of <a href="https://minecraft.net/en-us/store/minecraft/?ref=m#owned">Minecraft</a></li>
                            <li>A computer capable of Minecraft (4GB of memory required for engineering server)</li>
                            <li><a href="https://wiki.mumble.info/wiki/Main_Page">Mumble</a>, if you wish to talk while playing (same hostname, port 64738)</li>
                            <li>Internet</li>
                        </ol>
                        <a class="btn btn-light btn-xl js-scroll-trigger" href="#instructions">Get Started!</a>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Instructions -->
        <section class="bg-info minh" id="instructions">
            <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto text-center">
                            <h2 class="section-heading text-white">General Instructions</h2>
                            <hr class="light my-4">
                            <p class="text-faded mb-8">Server Hostname: minecraft.blackhawkelectronics.com</p>
                        </div>
                    </div>
                
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <h2 class="section-heading text-white">Engineering Instructions</h2>
                        <hr class="light my-4">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10">
                        <ol>
                            
                            <li>Login and run the current version the servers are running (1.12.2) and then close Minecraft and the Launcher</li>
                            <li>Download the latest forge installer <a href="http://files.minecraftforge.net/">here</a></li>
                            <li>Run the launcher, modify settings for 4GB of memory for the new forge profile, run the new forge profile, and quit.</li>
                            <li>Download the mods using <a href="/mods/downmod.py">this</a> python program (requires <a href="https://www.python.org/downloads/">Python</a> 2 or 3 and the requests module installed via <code>pip install requests</code>)</li>
                            <li>Run the forge profile and join the server!</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js" integrity="sha256-tw5/wId1F+4LyUF2ZJnpRoHNXJ66jrK3IGhfq4LhLjY=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="/scroller.js"></script>
        <script>
                function checkServers(){
                    $('.statusdot').each(function(){
                        const dot = $(this);
                        $.ajax({
                            url: '/server/' + dot.data('target') + '/status',
                            method: 'GET',
                            success: function(data){
                                if(data.online == false) {
                                    dot.removeClass('online').addClass('offline').text("");
                                } else if (data.online == true) {
                                    dot.removeClass('offline').addClass('online').text(data.current_players);
                                } else {
                                    dot.removeClass('offline online').attr('title','Unknown').text("");
                                }
                            },
                            statusCode: {
                                404: function(){
                                    dot.removeClass('online').addClass('offline').attr('title','Does not exist');
                                }
                            }
                        });
                    });
                }

            $(function(){
                checkServers();
                var intervalstatus = setInterval(checkServers,60000+Math.floor(Math.random()*10000));
                });
        </script>
    </body>
</html>
