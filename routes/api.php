<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group(['prefix' => 'v1'], function() {
    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    // Anonymous API calls
    Route::get('/server/featured','ServerController@getFeatured');
    Route::get('/server/{server}','ServerController@show');
    Route::get('/server/{server}/latest','ServerStatusController@latest'); // this one may need authorization, depending on server

    // Authenticated API calls
});

Route::fallback(function() {
    return response("Not Found", 404);
});
