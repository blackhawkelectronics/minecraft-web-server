<?php

namespace App\Facades;

use Exception;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Log;

class Minecraft {
    
    /**
     * Ping a Minecraft server
     */
    public static function ping(string $address, int $port = 25565): array
    {
        $DATA_SIZE = 512;    // this will hopefully suffice since the MotD should be <=59 characters
        $NUM_FIELDS = 6;     // number of values expected from server

        $status = [
            'online' => false,
            'reason' => 'Unknown',
        ];

        try
        {
            // ToDo: Add timeout 
            $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
            if($socket === false) {
                throw new Exception("Could not open.");
            }

            socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array(
                "sec" => 30,
                "usec" => 0
            ));
            socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, array(
                "sec" => 15,
                "usec" => 0
            ));

            if(@socket_connect($socket, $address, $port) === false) {
                throw new Exception("Could not connect.");
            }

            $payload = "\xFE\x01";
            $time = microtime(true);
            socket_write($socket, $payload, strlen($payload));

            $raw_data = socket_read($socket, $DATA_SIZE);
            $time = microtime(true) - $time;

            socket_close($socket);
        } catch(Exception $e) {
            $status['reason'] = "Exception: " . $e->getMessage();
            return $status;
        }
        
        $data = explode("\x00\x00\x00", $raw_data);
        if(isset($data) && sizeof($data) >= $NUM_FIELDS)
        {
            $status['online'] = true;
            $status['version'] = str_replace("\0","",$data[2]);
            $status['motd'] = str_replace("\0","",$data[3]);
            $status['current_players'] = (int)str_replace("0","",$data[4]);
            $status['max_players'] = (int)str_replace("\0","",$data[5]);
            $status['ping'] = floor($time);
            $status['reason'] = null;
            $status['raw'] = $data;
        } else {
            $status['reason'] = "No Data Recieved.";
        }
        
        return $status;
    }
}