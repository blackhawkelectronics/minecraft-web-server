<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Server;
use App\Http\Resources\ServerStatusResource;

class ServerStatusController extends Controller
{
    public function latest(Server $server) {
        if(!$server->checks_enabled || is_null($server->checkhostname)) {
            return response("Checks disabled for this server.", 400);
        }
        return new ServerStatusResource($server->latestStatus);
    }
}
