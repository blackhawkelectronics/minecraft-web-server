<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Server;
use App\Http\Resources\ServerResource;
use App\Http\Resources\ServerCollection;

class ServerController extends Controller
{
    private function everyOther($string) {
        $output = '';
        for($i = 0; $i < strlen($string); $i += 2) {
            $output .= $string[$i];
        }
        return $output;
    }

    public function index() {
        return new ServerCollection(Server::all());
    }

    public function getFeatured() {
        return new ServerCollection(Server::where('featured', true)->limit(20)->get());
    }

    public function create() {
        
    }

    public function store(Request $request) {
        $validated = $this->validate($request,[
            'name' => 'required',
            'check' => 'boolean',
            'checkhostname' => 'required_if:check,true',
            'checkport' => 'required_if:check,true',
            'hostname' => [
                'required',
                'regex:((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}|[a-zA-Z][a-zA-Z0-9.\-]+)',
                'max:255',
            ],
        ]);
        $server = new Server();
        $server->name = $request->input('name');
        $server->check = $request->input('check') ?? false;
        $server->hostname = $request->input('hostname');
        $server->save();
        return $server;
    }

    public function show(Server $server) {
        return new ServerResource($server);
    }

    public function edit(Server $server) {

    }

    public function update(Server $server, Request $request) {

    }

    public function destroy(Server $server, Request $request) {
        $user = $request->user();
        if($user->id == $server->owner_id || $user->role == 'Admin'){
            $server->delete();
            return response('',204);
        }
        return response('',403);
    }
}
