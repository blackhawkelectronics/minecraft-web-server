<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServerStatus extends Model
{
    use HasFactory;

    protected $protected = [
        'id'
    ];

    protected $casts = [
        'players' => 'array',
    ];

    public function server() {
        return $this->belongsTo(Server::class);
    }
}
