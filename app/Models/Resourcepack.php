<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resourcepack extends Model
{
    public function modpacks(){
        return $this->belongsToMany('App\Modpack')->withPivot('version','scope');
    }
}
