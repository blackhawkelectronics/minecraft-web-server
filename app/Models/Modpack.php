<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modpack extends Model
{
    public function mods(){
        return $this->belongsToMany('App\Mod')->withPivot('version','scope');
    }
    public function resourcepacks(){
        return $this->belongsToMany('App\Resourcepack')->withPivot('version','scope');
    }
    public function owner(){
        return $this->belongsTo('App\User');
    }
}
