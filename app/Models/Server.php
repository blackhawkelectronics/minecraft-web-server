<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $with = [
        'latestStatus',
    ];

    public function status() {
        return $this->hasMany(ServerStatus::class);
    }

    public function latestStatus() {
        return $this->hasOne(ServerStatus::class)->latest();
    }
}
