<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Illuminate\Support\Facades\Hash;

class MakeUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Wizard to create a user from the commandline';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name     = $this->ask("What is the user's name?");
        $email    = $this->ask("What is the user's email?");
        $password = $this->secret('Passsword?');
        $level    = $this->choice('What level will this user have?', ['User','Moderator','Admin'],0);

        if(User::where('email',$email)->count() > 0){
            $this->error('E-Mail already in use!');
        }
        
        $user = new User();
        $user->name = $name;
        $user->password = Hash::make($password);
        $user->email = $email;
        $user->role = $level;
        $user->save();
        $this->info("Created user for $email");
    }
}
