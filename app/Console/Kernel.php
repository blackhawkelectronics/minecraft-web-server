<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Bus;
use App\Models\Server;
use App\Jobs\ServerPing;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function(){
            $servers = Server::where('checks_enabled', true)->whereNotNull('checkhostname')->select(['id','checkhostname','checkport'])->get();
            $jobs = [];
            foreach($servers as $server) {
                $jobs[] = new ServerPing($server);
            }
            Bus::batch($jobs)->dispatch();
        })->everyFiveMinutes()->name("Ping Servers");
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
