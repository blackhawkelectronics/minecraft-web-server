<?php

namespace App\Jobs;

use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\Server;
use App\Models\ServerStatus;
use App\Facades\Minecraft;

class ServerPing implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Batchable;

    private ?ServerStatus $serverStatus = null;

    /**
     * The number of seconds after which the job's unique lock will be released.
     */
    public int $uniqueFor = 300;

    /**
     * The number of seconds the job can run before timing out.
     */
    public int $timeout = 120;

    /**
     * Indicate if the job should be marked as failed on timeout.
     */
    public bool $failOnTimeout = false;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        private Server $server
    ) {
        //
    }

    /**
     * The unique ID of the job.
     *
     * @return string
     */
    public function uniqueId()
    {
        return $this->server->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = Minecraft::ping($this->server->checkhostname, $this->server->checkport ?? 25565);
        $status = new ServerStatus;
        $status->server()->associate($this->server);
        $status->online = $data['online'];
        $status->reason = $status['reason'];
        if($data['online']) {
            $status->version = $data['version'];
            $status->players = [];
            $status->count = $data['current_players'];
            $status->max_players = $data['max_players'];
            $status->ping = $data['ping'];

            if(!empty($data['motd'])) {
                $this->server->motd = $data['motd'];
            }
        }
        $status->save();
        $this->server->save();
        $this->serverStatus = $status;
    }
}
