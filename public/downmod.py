#!/usr/bin/python3
from __future__ import print_function
import sys
import os
import requests
import re
import webbrowser
import glob

class bcolors:
    # This is not working in windows
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    # This is not working in windows
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

class modloader:         
    def __init(self,url,folder=""):
        self.url = url

        if folder == "":
            if sys.platform == "linux" or sys.platform == "linux2":
                self.basepath = os.path.join(os.path.expanduser('~'), ".minecraft")
                self.os = "linux"
                import signal # Signal has not been working in windows either
                def signal_handler(sig, frame):
                    print(bcolors.FAIL,"\nExiting...",bcolors.ENDC)
                    sys.exit(0)
                signal.signal(signal.SIGINT, signal_handler)
                signal.signal(signal.SIGQUIT, signal_handler)
            elif sys.platform == "win32":
                self.basepath = os.path.join(os.getenv("APPDATA"), '.minecraft')
                self.os = "windows"
            elif sys.platform == "darwin":
                self.basepath = os.path.join(os.path.expanduser('~'),"Library/Application Support/minecraft")
                self.os = "macos"
            else:
                print("Unsupported OS, ",platform)
                exit()

        print("Current location is " + location)

        currentVersion = requests.get(modsurl + "current.txt")
        currentVersionValidate = re.search( '(\d\.\d\d\.\d)', currentVersion)

        if currentVersionValidate:
            print("Current version is " + currentVersionValidate.group())
            self.currentVersion = currentVersionValidate.group()
        else:
            print("Version Validation Error")
            exit()

        self.config = requests.get(modsurl + currentVersionValidate.group() + ".json").json()
        if not "schema" in config or config["schema"] == 3:
            print(bcolors.FAIL,"Downloaded wrong schema type. Please make sure you are using the correct downmod.py script!",bcolors.ENDC)
            sys.exit(2)
        self.mainmodpath = os.path.join(self.basepath,"mods")
        self.modpath = os.path.join(self.mainmodpath,self.currentVersion)
        self.resourcepackpath = os.path.join(self.basepath,"resourcepacks")

    def check_folders(self):
        # Check for mods folder
        if not os.path.isdir(self.mainmodpath):
            print("Mods directory is missing! Please install forge and run it at least once.")
            exit()
    def load_requirements(self,name,data):
        downloads = []
        deletions = []
        
        submods = []
        desiredname = name
        if "separator" in data:
            desiredname += data["separator"]
        else:
            desiredname += "-"
        regex = desiredname
        desiredname += data["version"]
        regex += "*"
        if "submods" in data:
            desiredname += '-$submod$'
            regex += '-$submod$'
            submods = [desiredname + "-" + x for x in data["submods"]]
        if "post" in data:
            desiredname += data["post"] 
            regex += data["post"] 
        desiredname = desiredname + ".jar"
        if len(submods) == 0: # Mods with no submods
            fpath = os.path.join(modpath,desiredname)
            if os.path.isfile(fpath):
                # Skip downloading if the path exists already
                print(bcolors.HEADER + mod.rjust(longestmodname)  +  " : " + bcolors.ENDC + bcolors.OKGREEN + "Up to date"+bcolors.ENDC)
                continue
            if "url" in data:
                downloads[desiredname] = data['url']
            else:
                downloads[desiredname] = modsurl + currentVersion + "/" + desiredname
            print(bcolors.HEADER + mod.rjust(longestmodname) +  " : " + bcolors.ENDC + bcolors.WARNING + "Downloading "+data['version']+bcolors.ENDC)
            extrafiles.extend(filter(None,[None if os.path.basename(x) == desiredname else x for x in glob.glob(os.path.join(modpath,regex) + ".jar")]))
            # TODO: When there is a version that is greater, don't update
        else:
            missingsubmods = False
            for submod in data['submods']:
                fpath = os.path.join(modpath,desiredname.replace("$submod$",submod))
                if not os.path.isfile(fpath):
                    missingsubmods = True
            if missingsubmods:
                print(bcolors.HEADER + mod.rjust(longestmodname)  +  " : " + bcolors.ENDC + bcolors.WARNING + "Updating"+bcolors.ENDC)
            else:
                print(bcolors.HEADER + mod.rjust(longestmodname)  +  " : " + bcolors.ENDC + bcolors.OKGREEN + "Up to date"+bcolors.ENDC)
            for submod in data['submods']:
                desiredsubname = desiredname.replace("$submod$",submod)
                fpath = os.path.join(modpath,desiredsubname)
                if not os.path.isfile(fpath):
                    print(" "*(longestmodname+2),"- ",submod)
                    downloads[desiredsubname] = modsurl + currentVersion + "/" + desiredsubname
                extrafiles.extend(filter(None,[None if os.path.basename(x) == desiredsubname else x for x in glob.glob(os.path.join(modpath,regex.replace("$submod$",submod)) + ".jar")]))

        return (downloads,deletions)

    def update_mods(self,downloads,deletions):
        pass

    def load_resourcepack(self,config):
        pass

if sys.platform == "linux" or sys.platform == "linux2":
    import signal # Signal has not been working in windows
    def signal_handler(sig, frame):
        print(bcolors.FAIL,"\nExiting...",bcolors.ENDC)
        sys.exit(0)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

# Begining of process
modmanager = modloader("https://minecraft.blackhawkelectronics.com/mods/")
modmanager.check_folders()





if operatingsystem == "linux": # Yet another thing that hasn't been working in windows. crashes at re.sub and then version=
    # Check for highest forge version. folder "<mchome>/versions/" Regex: "<version>-forge<version>-<forge version>"
    versionpath = os.path.join(location,'versions') + "/"+currentVersion+"-forge"+currentVersion+"-"
    forgeversions = [re.sub('^'+versionpath,"",x) for x in glob.glob(versionpath + "*")] # Windows may need to have .replace('\\','\\\\')
    wantedforgeversion = [int(x) for x in config["min_forge_version"].split(".")]
    goodforge = 0;
    for version in forgeversions:
        version = [int(x) for x in version.split(".")]
        if cmp(wantedforgeversion,version) <= 0:
            goodforge = 1
            break

    if goodforge == 0:
        print("Need forge version of at least " + config["min_forge_version"])
        print("Only found the following versions: " + ", ". join(forgeversions))
        print("Please install that version or newer of Forge.")
        print("https://files.minecraftforge.net")
        print("Make sure to ignore the ads and click the \"Skip Ad\" Link in the top right corner when downloading")
        exit()

if not os.path.exists(modpath):
    os.makedirs(modpath)

# Check the required client jars and make sure that they are in
downloads = {}
longestmodname = 0
extrafiles = []
for mod in config["required"]:
    if len(mod) > longestmodname:
        longestmodname = len(mod)
for mod in config["required"]:
    (downloads,extrafiles) = load_required(mod,config["required"][mod])

if len(downloads) > 0:
    if query_yes_no("Do you wish to download these updates?") == False:
        exit()

    # Download Files
    for file, url in downloads.items():
        fpath = modpath + "/" + file
        print("Downloading " + url + " to " + fpath, end =" ")
        r = requests.get(url)
        if r.status_code == 200:
            with open(fpath,'wb+') as f:
                f.write(r.content)
            print(bcolors.OKGREEN, "OK", bcolors.ENDC)
        else:
            print(bcolors.FAIL, "Failed. Got status ",r.status_code,bcolors.ENDC)

# Delete outdated managed files
if len(extrafiles) > 0:
    for file in extrafiles:
        if os.path.isfile(file):
            print("Delete " + file)
            os.remove(file)
if "optionalclient" in config:
    for mod in config["optionalclient"]:
        # Calculate desired name
        submods = []
        desiredname = mod
        data = config["optionalclient"][mod]
        if "separator" in data:
            desiredname += data["separator"]
        else:
            desiredname += "-"
        desiredname += data["version"]
        if "submods" in data:
            desiredname += '-$submod$'
            submods = [desiredname + "-" + x for x in data["submods"]]
        if "post" in data:
            desiredname += data["post"] 
        desiredname = desiredname + ".jar"
        if len(submods) == 0: # Mods with no submods
            fpath = os.path.join(modpath,desiredname)
            if os.path.isfile(fpath):
                # Skip downloading if the path exists already
                print(bcolors.HEADER + mod.rjust(longestmodname)  +  " : " + bcolors.ENDC  + "Optional: " + bcolors.OKGREEN + "Up to date"+bcolors.ENDC)
                continue
            if "url" in data:
                downloads[desiredname] = data['url']
            else:
                downloads[desiredname] = modsurl + currentVersion + "/" + desiredname
            print(bcolors.HEADER + mod.rjust(longestmodname) +  " : " + bcolors.ENDC + "Optional: " + bcolors.WARNING  + "Missing "+data['version']+bcolors.ENDC)
            if "dlpage" in data:
                print("Download page at", data["dlpage"])
            elif "url" in data:
                print("Download at", data["url"])
            if "requirements" in data:
                for x in data['requirements']:
                    data2 = data["requirements"][x]
                    if "url" in data2:
                        print("  - Requires",x,data2['url'])
                    else:
                        print("  - Requires",x,modsurl + currentVersion + "/" + x + data2['separator'] if 'separator' in data2 else '-' + data2["version"] + data2['post'] if 'post' in data2 else "" + ".jar")
        else:
            missingsubmods = False
            for submod in data['submods']:
                fpath = os.path.join(modpath,desiredname.replace("$submod$",submod))
                if not os.path.isfile(fpath):
                    missingsubmods = True
            if missingsubmods:
                print(bcolors.HEADER + mod.rjust(longestmodname)  +  " : " + bcolors.ENDC + "Optional: " + bcolors.WARNING + "Missing"+bcolors.ENDC)
            else:
                print(bcolors.HEADER + mod.rjust(longestmodname)  +  " : " + bcolors.ENDC + "Optional: " +  bcolors.OKGREEN + "Up to date"+bcolors.ENDC)
            for submod in data['submods']:
                desiredsubname = desiredname.replace("$submod$",submod)
                fpath = os.path.join(modpath,desiredsubname)
                if not os.path.isfile(fpath):
                    print(" "*(longestmodname+2),"- ",submod)


print("\n",bcolors.OKBLUE,u'\u2713'," Done",bcolors.ENDC)

# TODO: Several things are done many times with each dictionary of mods.
# functions to create: check if mod exists, check if there are extra files, generating the url or filename
# Maybe turn each mod and requirement into a class that is much more easily accessible
