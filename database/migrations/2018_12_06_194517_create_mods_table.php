<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('currentVersion');
            $table->text('description');
            $table->string('giturl');
            $table->jsonb('versions'); 
            $table->char('separator')->default('-');
            $table->string('post')->nullable();
            $table->integer('scope')->default(0); // 0 = client and server
            $table->string('copyright')->default("");
            $table->timestamps();
        });
        Schema::create('mod_modpack',function (Blueprint $table) {
            $table->integer('mod_id');
            $table->integer('modpack_id');
            $table->string('version');
            $table->integer('scope')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_modpack');
        Schema::dropIfExists('mods');
    }
}
