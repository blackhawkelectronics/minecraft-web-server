<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('type')->default('vanilla');
            $table->integer('modpack_id')->nullable();
            $table->string('checkhostname')->nullable();
            $table->integer('checkport')->nullable();
            $table->string('internalhostname')->nullable();
            $table->string('internalport')->nullable();
            $table->string('externalhostname');
            $table->integer('externalport')->nullable();
            $table->boolean('onProxy');
            $table->string('proxyName')->nullable();
            $table->integer('owner_id');
            $table->foreign('owner_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
