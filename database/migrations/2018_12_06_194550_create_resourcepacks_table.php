<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcepacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resourcepacks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('currentversion');
            $table->string('clientlocation')->nullable();
            $table->string('serverlocation')->nullable();
            $table->jsonb('versions');
            $table->integer('scope')->default(0);
            $table->timestamps();
        });
        Schema::create('modpack_resourcepack', function (Blueprint $table) {
            $table->integer('modpack_id');
            $table->integer('resourcepack_id');
            $table->string('version');
            $table->integer('scope')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modpack_resourcepack');
        Schema::dropIfExists('resourcepacks');
    }
}
