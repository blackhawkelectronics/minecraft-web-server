<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('server_statuses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('server_id');
            $table->boolean('online');
            $table->string('reason')->nullable();
            $table->string('version')->nullable();
            $table->integer('count')->nullable();
            $table->jsonb('players')->nullable();
            $table->integer('max_players')->nullable();
            $table->integer('ping')->nullable();
            $table->timestamps();
        });

        Schema::table('servers',function(Blueprint $table){
            $table->boolean('checks_enabled')->default(true);
            $table->string('motd')->default('');
            $table->boolean('is_proxy')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('server_statuses');

        Schema::table('servers',function(Blueprint $table){
            $table->dropColumn('checks_enabled');
            $table->dropColumn('motd');
            $table->dropColumn('is_proxy');
        });
    }
};
